<?php
/**
 * Created by PhpStorm.
 * User: haier
 * Date: 2018-6-04
 * Time: 20:25
 */

define('ROW', 5);
define('COL', 5);

//现状的od表
$od_now = array();
$od_now[1] = [null, 1000, 500, 300, 200, 100];
$od_now[2] = [null, 450, 1500, 500, 100, 150];
$od_now[3] = [null, 350, 200, 800, 300, 50];
$od_now[4] = [null, 150, 150, 300, 700, 50];
$od_now[5] = [null, 80, 100, 50, 70, 500];
//var_dump($od_now);

//各Oi的值 行数值求和
$o = array();
for ($i = 1; $i < 6; $i++) {
    $o[$i] = array_sum($od_now[$i]);
}

//各Di的值 列数值求和
$d = array();
for ($i = 1; $i < 6; $i++) {
    $d[$i] = $od_now[1][$i] +
        $od_now[2][$i] +
        $od_now[3][$i] +
        $od_now[4][$i] +
        $od_now[5][$i];
}

//现状的出现路阻表
$c_now = array();
$c_now[1] = [null, 5, 10, 18, 20, 30];
$c_now[2] = [null, 12, 4, 10, 25, 20];
$c_now[3] = [null, 15, 10, 8, 15, 35];
$c_now[4] = [null, 20, 25, 15, 10, 30];
$c_now[5] = [null, 35, 25, 35, 35, 12];
//var_dump($c_now);
//进行标定
$r = 1;
$b = [null, 1, 1, 1, 1, 1];
$a = [null, 1, 1, 1, 1, 1];
$flag_a = [null, 0, 0, 0, 0, 0];
//$flag_a = 0;
$flag_b = [null, 0, 0, 0, 0, 0];
//$flag_b = 0;
$flag_num = array_sum($flag_a) + array_sum($flag_b);
//var_dump($od_now);
//var_dump($c_now);
//var_dump($o);
//var_dump($d);

try {
    for($r=1,$step=0.001;;) {
        for ($counter = 0; $flag_num!=10; $counter++) {
            unset($a_new);
            unset($b_new);
            //取b标定a
            $a = calibA($c_now, $r, $b, $d, $flag_a, $a);
            //取新的a标定b
//        var_dump($a_new);
            $b_new = calibB($c_now, $r, $a, $o, $flag_b, $b);
            //取新b标定a
            $a_new = calibA($c_now, $r, $b, $d, $flag_a, $a);

            //参数的收敛判断
            unset($flag_a);
            unset($flag_b);
            $flag_a = isConv($a_new, $a);
            $flag_b = isConv($b_new, $b);
            //判断是否两个都符合收敛标准
//        $flag_num = array_sum($flag_a) + array_sum($flag_b);
//        if ($flag_num == 10) {
            if ($flag_a + $flag_b == 2) {
                unset($a);
                $a = $a_new;
                break;
            }

            //新的迭代赋值
            unset($a);
            unset($b);
            $a = $a_new;
            $b = $b_new;
            //判断是否两个都符合收敛标准
//        $flag_num = array_sum($flag_a) + array_sum($flag_b);
//        var_dump($a);
//        var_dump($b);
//        var_dump($flag_num);
        }
        //标定成功 输出参数
        for ($i = 1; $i <= ROW; $i++) {
            echo "a[$i]=$a[$i]     ||   b[$i]=$b[$i]";
            echo "<br/>";
        }
        echo "counter = $counter";
        echo "<br/>";

        //标定结束后，使用求GM分布的qij
        $q = array();
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                $q[$i][$j] = $a[$i] * $b[$j] * $o[$i] * $d[$j] * pow($c_now[$i][$j], -1 * $r);
            }
        }
        //输出GM分布情况
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                echo "q[$i][$j] = " . $q[$i][$j] . "  ||  ";
            }
            echo "<br/>";
        }
        //求GM分布的平均行程时间
        //求分母 Σq
        $sum_q = 0;
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                $sum_q = $q[$i][$j] + $sum_q;
            }
        }
        //求分子 Σqc
        $sum_qc = 0;
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                $sum_qc = $sum_qc + $q[$i][$j] * $c_now[$i][$j];
            }
        }
        $avg_c_gm = $sum_qc / $sum_q;
        echo "GM分布平均行程时间：" . $avg_c_gm;
        echo "<br/>";

        //求现状平均行程时间

//求分母 Σod
        $sum_od = 0;
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                $sum_od = $od_now[$i][$j] + $sum_od;
            }
        }
        //求分子 Σod c
        $sum_odc = 0;
        for ($i = 1; $i <= ROW; $i++) {
            for ($j = 1; $j <= COL; $j++) {
                $sum_odc = $sum_odc + $od_now[$i][$j] * $c_now[$i][$j];
            }
        }
        $avg_c_now = $sum_odc / $sum_od;
        echo "od现状分布平均行程时间：" . $avg_c_now;
        echo "<br/>";

        //计算误差
        $dif = ($avg_c_gm - $avg_c_now) / $avg_c_now;
        echo "误差为：" . ($dif * 100) . "%";
        echo "<br/>";
//  var_dump($q);
        if ($dif < 0.03 && $dif > (-1 * 0.03)) {
            echo "符合标定误差要求";
        } else if ($dif > 0) {
            echo "不符合标定误差要求，需要调整γ变大";
            $r = $r + $step;
        } else {
            echo "不符合标定误差要求，需要调整γ变小";
            $r = $r - $step;
        }
        echo "<br/>";
    }


    //预测未来分布
//未来路阻表
    $c_future = array();
    $c_future[1] = [null, 5, 10, 12, 15, 20];
    $c_future[2] = [null, 10, 4, 8, 20, 20];
    $c_future[3] = [null, 10, 10, 7, 10, 30];
    $c_future[4] = [null, 15, 20, 10, 8, 20];
    $c_future[5] = [null, 30, 20, 30, 30, 10];
//未来od量
$o_future = [null,5000,4000,3000,2500,2000];
$d_future = [null,4000,4500,3500,2500,2000];
//计算
    $od_future = array();
    for($i=1;$i<=ROW;$i++){
        for($j=1;$j<=COL;$j++){
            $od_future[$i][$j] = $a[$i]*$b[$j]*$o_future[$i]*$d_future[$j]*pow($c_future[$i][$j],-1*$r);
        }
    }

    echo "γ=：$r";
    echo "<br/>";
    //输出未来分布情况
    for($i=1;$i<=ROW;$i++){
        for($j=1;$j<=COL;$j++){
            echo "od'[$i][$j] = ".$od_future[$i][$j]/2.3 ."  ||  ";
        }
        echo "<br/>";
    }
    $o_cul=[null,0,0,0,0,0];
    for($i=1;$i<=ROW;$i++){
        for($j=1;$j<=COL;$j++){
            $o_cul[$i] =  $o_cul[$i] + $od_future[$i][$j];
        }
    }
var_dump($o_cul);

} catch (Exception $e) {
    echo $e->getMessage();
}


function isConv(array $new, array $old)
{
    if (sizeof($new) != sizeof($old)) {
        throw new Exception("params array not same size");
    }

    for ($i = 1; $i <= ROW; $i++) {
        $res = ($new[$i]-$old[$i])/$old[$i];
        if ($res > -0.05 && $res < 0.05) {
            //符合收敛的a参数标记为1
            $flag[$i] = 1;
        } else {
            $flag[$i] = 0;
        }
    }
    //返回参数都符合收敛标准的鉴定情况
    return $flag;
}

function isConv2(array $new, array $old)
{
    $vaild_num = 0;
    if (sizeof($new) != sizeof($old)) {
        throw new Exception("params array not same size");
    }

    for ($i = 1; $i <= ROW; $i++) {
        $res = ($new[$i]-$old[$i])/$old[$i];
        if ($res > -0.05 && $res < 0.05) {
            $vaild_num++;
        }
    }

    return ($vaild_num==ROW)?1:0;

}

//用B标定A
function calibA($c_now, $r, $b, $d, $flag,$old)
{
    unset($new);
    $new[0] = null;
    for ($i = 1; $i <= ROW; $i++) {

        $new[$i] = 0;
        //累加求a
        //标定前检查 是否需要标定
        for ($j = 1; $j <= ROW; $j++) {
//            if ($flag[$i] == 1) {//不需要重新标定
//                $new[$i] = $old[$i];
//                $new[$i] = pow($new[$i],-1);
//                break;
//            } else {
                $new[$i] = $new[$i] + pow($c_now[$i][$j], -1*$r)*($b[$j] * $d[$j]);
//            }
        }
        $new[$i] = pow($new[$i],-1);
    }

    if (empty($new) || sizeof($new) != ROW + 1) {
        var_dump($new);
        throw new Exception("new empty", 500);
    }
    return $new;
}


//用A标定B
function calibB($c_now, $r, $a, $o, $flag, $old)
{
    unset($new);
    $new[0] = null;
    for ($j = 1; $j < 6; $j++) {
        $new[$j] = 0;
        //累加求b
        for ($i = 1; $i <= ROW; $i++) {
//            if ($flag[$j] == 1) {//不需要重新标定
//                $new[$j] = $old[$j];
//                $new[$j] = pow($new[$j],-1);
//                break;
//            } else {
                $new[$j] = $new[$j] + pow($c_now[$i][$j], -1*$r) * ($a[$i] * $o[$i]);
//            }
        }
        $new[$j] = pow($new[$j],-1);

    }


    if (empty($new) || sizeof($new) != ROW + 1) {
        var_dump($new);
        throw new Exception("new empty", 500);
    }
    return $new;
}